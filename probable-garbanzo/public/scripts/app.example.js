class App {
  constructor() {
    // this.clearButton = document.getElementById("clear-btn");
    // this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.buttonCari = document.getElementById("btnCari");
  }

  async init() {
    await this.load();
    // Register click listener
    // this.clearButton.onclick = this.clear;
    this.buttonCari.onclick = this.run;
  }

  run = () => {
    let capacity = document.getElementById("passanger").value;
    let dateCari = document.getElementById("date").value;
    let timeCari = document.getElementById("time").value;
    let driver = document.getElementById("driver").value;

    // console.log(capacity);
    // console.log(dateCari);
    // console.log(timeCari);
    // console.log(driver);
    let newDate = new Date(`${dateCari}T${timeCari}Z`);
    if (
      capacity === "" ||
      dateCari === "" ||
      timeCari === "" ||
      driver === ""
    ) {
      alert("Isi semua bos!!!");
    } else {
     this.clear();
      Car.list.forEach((car) => {
        if (
          car.capacity >= capacity &&
          car.availableAt >= newDate &&
          car.available === true
        ) {
          const node = document.createElement("div");
          node.innerHTML = car.cetak();
          this.carContainerElement.append(node);
        } 
      });
    }
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
